/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userInfo()
	{
		let userFullName = prompt("Enter your Full Name:");
		let userAge = prompt("Enter your Age:");
		let userLocation = prompt("Enter your Location:");

		console.log("Hello, "+ userFullName);
		console.log("You are "+userAge+" years old.");
		console.log("You live in "+userLocation);
	}
	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favBandArtist()
	{
		let faveArtist1 = prompt("Enter your first favorite band/ music artist:");
		let faveArtist2 = prompt("Enter your second favorite band/ music artist:");
		let faveArtist3 = prompt("Enter your third favorite band/ music artist:");
		let faveArtist4 = prompt("Enter your fourth favorite band/ music artist:");
		let faveArtist5 = prompt("Enter your fifth favorite band/ music artist:");
		
		console.log("1. "+faveArtist1); //EXO
		console.log("2. "+faveArtist2); //Taylor Swift
		console.log("3. "+faveArtist3); //Panic! AT the Disco
		console.log("4. "+faveArtist4); //Roadtrip
		console.log("5. "+faveArtist5); //Calum Scott
	}
	favBandArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function faveMovies()
	{
		let faveMovie1 = prompt("Enter your first favorite movie:"); //
		let faveMovie2 = prompt("Enter your second favorite movie:"); //
		let faveMovie3 = prompt("Enter your third favorite movie:"); //
		let faveMovie4 = prompt("Enter your fourth favorite movie:"); //
		let faveMovie5 = prompt("Enter your fifth favorite movie:"); //

		console.log("1. "+faveMovie1);
		console.log("Rotten Tomato Ratings: 85%"); //Infinity War
		console.log("2. "+faveMovie2);
		console.log("Rotten Tomato Ratings: 91%"); //Enola Holmes
		console.log("3. "+faveMovie3);
		console.log("Rotten Tomato Ratings: 100%"); //3 Idiots
		console.log("4. "+faveMovie4);
		console.log("Rotten Tomato Ratings: 90%%");//Coraline
		console.log("5. "+faveMovie5);
		console.log("Rotten Tomato Ratings: 87%");//Kung Fu Panda 1/2/3

	}
	faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);

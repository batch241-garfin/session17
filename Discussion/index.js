// Functions //

	// Functions in javascript are lines/ block of codes
	// that tells our device/ application to perform a 
	//certain task when called/ invoked.
	// Functions are mostly created to create complicated
	// tasks to run several lines of code in succession.

	// Functions are also used to prevent repeating
	// lines/ blocks of codes that perform the same
	// task/ function.

// Function Declaration

	// Function statements defines a function with the
	// specified parameters.

	// Syntax:
		// function functionName ()
		// {
			// codeblock (statement) 
		// }

	// function keyword - used to defined a javascipt
	// functions.
	// functionName - the function name.
	// functionName are named to be able to use later
	// in the code.
	// function block ({}) - the statements which
	// compromise the body of the function.
	// This is where the code to be executed.


function printName()
{
	console.log("My name is Romhelson!");

}

// Function Invocation

	//The code of block and statements inside a
	// function is not immediately exected when the
	// function is defined.
	// The code block and statements inside a function
	// is executed when the function is invoked or called.

printName();

// Function Declaration vs. Function Expressions

	// Function Declaration
		//A function are not executed immediately. They
		// are saved for later used and will be executed later
		// when they are invoked.

	function declaredFunction()
	{
		console.log("Hello declaredFunction!");
	}
	declaredFunction();

	// Function Expressions

		// A function can also be stored in a variable.
		// This is called function expression.
	
		// A function expression is an anonymous function
		// assigned to the variableFunction.

	let variableFunction = function()
	{
		console.log("Hello variableFunction");
	}
	variableFunction();

		// Function Expression are always invoked or
		// called using a variable name.

	let functionExpression = function funcName ()
	{
		console.log("Hello other side!");
	}
	functionExpression();


	declaredFunction = function()
	{
		console.log("Updated declaredFunction");
	}
	declaredFunction();

// Function Scoping

	// JS variables has 3 types of variables
	// 1. local/ block scope
	// 2. global scope
	// 3. function scope

	//They can only be accessed inside of the function
	// they were declared in.

{
	let locaVar = "I am local variable";
	console.log(locaVar);
}
let globalVar = "I am a global variable";

console.log(globalVar);

function showNames ()
{
	// function scoped variables

	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}
showNames();

// Nested Function

	// Creating another function inside a function.

function myNewFunction ()
{
	let name = "Maria";

	function nestedFunction()
	{
		let nestedName = "Jose";
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();

// Function and Global Scoped Variables

	// Global Variable

let globalName = "Erven Joshua";

function myNewFunction2()
{
	let nameInside = "Jenno";
	console.log(globalName);
}
//console.log(nameInside);
myNewFunction2();

// Using Alert Method ( alert() )

//alert("Hello Romhel");

function showSampleAlert()
{
	alert("Hello Cutiiee");
}
//showSampleAlert();

console.log("I will only appear if the alert has been dismissed");

// Using Prompt

let samplePrompt = prompt("Enter your name: ");

console.log("Hello! "+ samplePrompt);

function printWelcomeMsg()
{
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name ");

	console.log("Hello "+ firstName +" "+ lastName + "!");
	console.log("Welcome to my page!");
}
//printWelcomeMsg();

// Function Naming Conventions

	// name your functions in small caps. Follows
	// camelCase when naming variables and functions.
	// Avoid generic names to avoid confusion.

function displayCarInfo()
{
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}
displayCarInfo();